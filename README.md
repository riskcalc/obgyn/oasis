# Predicting risk of obstetric anal sphincter injuries at the time of admission for delivery: A clinical prediction model

Douglas Luchristt, Ana Rebecca Meekins, Congwen Zhao, Chad Grotegut, Nazema Y Siddiqui, Brooke Alhanti, John Eric Jelovsek. BJOG. 2022 May 27. doi: 10.1111/1471-0528.17239. 

PMID: https://pubmed.ncbi.nlm.nih.gov/35621030/

This site provides information and source code for models in the above project.

This research was funded by Duke Hospital PDC Outcomes Research Team and American Urogynecologic Society Research Foundation.

The copyrights of this software are owned by Duke University. As such, two licenses to this software are offered:
(1) An open-source license under the GPLv2 license.
(2) A custom license with Duke University, for use without the GPLv2 restrictions.

As a recipient of this software, you may choose which license to receive the code under. Outside contributions to the Duke owned code base cannot be accepted unless the contributor transfers the copyright to those changes over to Duke University.
To enter a license agreement without the GPLv2 restrictions, please contact the Digital Innovations department at Duke Office of Licensing and Ventures (https://olv.duke.edu/software/) at olvquestions@duke.edu with reference to “OLV File No. 7484” in your email.

Please note that this software is distributed AS IS, WITHOUT ANY WARRANTY; and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
